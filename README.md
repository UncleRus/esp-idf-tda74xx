# esp-idf-tda74xx

ESP-IDF component for TDA7439/TDA7439D/TDA7440D digitally controlled audioprocessors.


## How to use

See example here: https://github.com/UncleRus/esp-idf-tda74xx-example
